import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name: string;
  hobbies: string[];
  showHobbies: boolean;

  constructor(){
    this.name = "Mi App Perfecta";
    this.hobbies = ['guitarra', 'jugar', 'ordenadores'];
    this.showHobbies = false;
  }

  toggleHobbies(){
    this.showHobbies = !this.showHobbies;
  }

  addHobbie(hobby){
    this.hobbies.push(hobby.value);
    hobby.value ='';
    return false;
  }
}
